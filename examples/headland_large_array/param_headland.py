from opentidalfarm import *

prob_params = MultiSteadySWProblem.default_parameters()

domain = FileDomain("mesh/headland.xml") 
prob_params.domain = domain

# Boundary conditions
tidal_amplitude = 5.
H = 40

bcs = BoundaryConditionSet()
eta_channel = "amp*cos(pi*t)"
eta_expr = Expression(eta_channel, t=Constant(0), amp=tidal_amplitude)
bcs.add_bc("eta", eta_expr, facet_id=1, bctype="strong_dirichlet")
bcs.add_bc("eta", Constant(0), facet_id=2, bctype="strong_dirichlet")
bcs.add_bc("u", Constant((0, 0)), facet_id=3, bctype="strong_dirichlet")

prob_params.bcs = bcs

# Equation settings
nu = Constant(300)
prob_params.viscosity = nu
prob_params.depth = Constant(H)
prob_params.friction = Constant(0.0025)
# Temporal settings
prob_params.start_time = Constant(0)
prob_params.finish_time = Constant(1)
prob_params.dt = prob_params.finish_time

# Define the farm
turbine = ThrustTurbine()

farm = RectangularFarm(domain,
                       site_x_start=9200.,
                       site_x_end=10800.,
                       site_y_start=2500.,
                       site_y_end=3500.,
                       turbine=turbine)
farm.add_regular_turbine_layout(num_x=16, num_y=32)

prob_params.tidal_farm = farm

# Set up the problem
problem = MultiSteadySWProblem(prob_params)

# Set up the solver parameters
sol_params = CoupledSWSolver.default_parameters()
sol_params.dump_period = 1
solver = CoupledSWSolver(problem, sol_params)

# Define the functional
functional = PowerFunctional(problem, cut_in_speed=1., cut_out_speed=2.5)

# Define the control
control = TurbineFarmControl(farm)

# Set up the reduced functional
rf_params = ReducedFunctional.default_parameters()
rf_params.save_checkpoints = True
rf = ReducedFunctional(functional, control, solver, rf_params)
print rf_params

lb, ub = farm.site_boundary_constraints()
ineq = farm.minimum_distance_constraints()
f_opt = maximize(rf,
                 bounds=[lb, ub],
                 constraints=ineq,
                 method='SLSQP',
                 options={'maxiter': 200})

print 'Final power is ', rf.last_j
