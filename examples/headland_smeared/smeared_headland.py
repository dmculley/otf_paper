from opentidalfarm import *

prob_params = MultiSteadySWProblem.default_parameters()

domain = FileDomain("mesh/headland.xml")
prob_params.domain = domain

# Boundary conditions
tidal_amplitude = 5.
H = 40

bcs = BoundaryConditionSet()
eta_channel = "amp*cos(pi*t)"
eta_expr = Expression(eta_channel, t=Constant(0), amp=tidal_amplitude, degree=4)
bcs.add_bc("eta", eta_expr, facet_id=1, bctype="strong_dirichlet")
bcs.add_bc("eta", Constant(0), facet_id=2, bctype="strong_dirichlet")
bcs.add_bc("u", Constant((0, 0)), facet_id=3, bctype="strong_dirichlet")

prob_params.bcs = bcs

# Equation settings
#prob_params.finite_element = finite_elements.mini
nu = Constant(300)
prob_params.viscosity = nu
prob_params.depth = Constant(H)
prob_params.friction = Constant(0.0025)
# Temporal settings
prob_params.start_time = Constant(0)
prob_params.finish_time = Constant(1)
prob_params.dt = prob_params.finish_time

# Define the farm
turbine = SmearedTurbine()
V = FunctionSpace(domain.mesh, "DG", 0)
farm = Farm(domain, turbine, function_space=V)

# Sub domain for inflow (right)
class FarmDomain(SubDomain):
    def inside(self, x, on_boundary):
        return (8800 <= x[0] <= 11200 and
                1400  <= x[1] <= 3800)

farm_domain = FarmDomain()
domains = MeshFunction("size_t", domain.mesh, domain.mesh.topology().dim())
domains.set_all(0)
farm_domain.mark(domains, 1)
site_dx = Measure("dx")[domains]
farm.site_dx = site_dx(1)
#plot(domains, interactive=True)

prob_params.tidal_farm = farm

# Set up the problem
problem = MultiSteadySWProblem(prob_params)

# Set up the solver parameters
sol_params = CoupledSWSolver.default_parameters()
sol_params.dump_period = 1
solver = CoupledSWSolver(problem, sol_params)

# Define the functional
functional = PowerFunctional(problem)

# Define the control
control = TurbineFarmControl(farm)

# Set up the reduced functional
rf_params = ReducedFunctional.default_parameters()
rf_params.save_checkpoints = True
rf_params.load_checkpoints = False
rf_params.automatic_scaling = None
rf = ReducedFunctional(functional, control, solver, rf_params)
print rf_params

f_opt = maximize(rf,
                 bounds=[0, 12.],
                 method='L-BFGS-B',
                 options={'maxiter': 200})

print 'Final power is ', rf.last_j

# Compute the total turbine friction
print "Total friction",  assemble(farm.friction_function*farm.site_dx(1))
