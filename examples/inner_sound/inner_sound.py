from opentidalfarm import *
import datetime


class OTF(object):

    def __init__(self,
                 channel_length, channel_width,
                 start_x, end_x,
                 start_y, end_y,
                 long_vel, trans_vel,
                 viscosity=600):

        self.channel_length = channel_length
        self.channel_width = channel_width
        self.start_x, self.end_x = start_x, end_x
        self.start_y, self.end_y = start_y, end_y
        self.long_vel, self.trans_vel = long_vel, trans_vel
        self.viscosity = viscosity

        print 'OpenTidalFarm initialised...'

    def run(self,
            nx=None, ny=None,
            turbine_positions=None,
            mode="sensitivity",
            other_info='None Run Info Provided',
            run_name='No name given',
            max_iterations=200):

        utm_zone = 30
        utm_band = 'V'

        domain = FileDomain("mesh/pentland_firth.xml")

        site_x = 1000.
        site_y = 600.

        site_x_start = 491744.
        site_y_start = 6.5015e+06

        inflow_x = self.long_vel
        inflow_y = self.trans_vel
        inflow_norm = (inflow_x**2 + inflow_y**2)**0.5
        inflow_direction = (inflow_x/inflow_norm, inflow_y/inflow_norm)
        print "inflow_direction: ", inflow_direction
	print "inflow_velocity", (inflow_x, inflow_y)

        # Specify boundary conditions.
        eta_expr = TidalForcing(grid_file_name='netcdf/gridES2008.nc',
                                data_file_name='netcdf/hf.ES2008.nc',
                                ranges=((-4.0,0.0), (58.0,61.0)),
                                utm_zone=utm_zone,
                                utm_band=utm_band,
                                initial_time=datetime.datetime(2001, 9, 18, 10,\
                                                               40),
                                constituents=['Q1', 'O1', 'P1', 'K1', 'N2',\
                                              'M2', 'S2', 'K2'], degree=1)
        bcs = BoundaryConditionSet()
        bcs.add_bc("eta", eta_expr, facet_id=1)
        bcs.add_bc("eta", eta_expr, facet_id=2)
        # The free-slip boundary conditions.
        bcs.add_bc("u", Constant((DOLFIN_EPS, DOLFIN_EPS)), facet_id=3,\
                   bctype="strong_dirichlet")

        # Next lets load the bathymetry from the NetCDF file
        bathy_expr = BathymetryDepthExpression(filename='netcdf/bathymetry.nc',
                                               utm_zone=utm_zone,
                                               utm_band=utm_band,
                                               domain=domain.mesh, 
                                               degree=1)

        # Set up variable viscosity for stability
        V = FunctionSpace(domain.mesh, 'CG', 1)
        dist = Function(V)
        File('mesh/dist.xml') >> dist

        class ViscosityExpression(Expression):
            def __init__(self, *args, **kwargs):
                self.dist_function = kwargs["dist_function"]
                self.nu_inside = kwargs["nu_inside"]
                self.nu_boundary = kwargs["nu_boundary"]
                self.dist_threshold = kwargs["dist_threshold"]

            def eval(self, value, x):
                if self.dist_function(x) > self.dist_threshold:
                    value[0] = self.nu_inside
                else:
                    value[0] = self.nu_boundary

        W = FunctionSpace(domain.mesh, "DG", 0)
        nu = ViscosityExpression(dist_function=dist, 
                                 dist_threshold=1000, 
                                 nu_inside=10., 
                                 nu_boundary=1e3, 
                                 degree=1)
        nu_func = interpolate(nu, W)

        # Set the shallow water parameters
        prob_params = SWProblem.default_parameters()
        prob_params.domain = domain
        prob_params.bcs = bcs
        prob_params.viscosity = nu_func
        prob_params.depth = bathy_expr
        prob_params.friction = Constant(0.0025)

	prob_params.start_time = Constant(0)
	prob_params.finish_time = Constant(0.5*60*60)
	prob_params.dt = Constant((12.5*60*60)/80)
	prob_params.theta = 1.0

	prob_params.initial_condition = Constant((DOLFIN_EPS, DOLFIN_EPS, 1))

        turbine = ThrustTurbine(water_depth=bathy_expr)
 	 #turbine = BumpTurbine()

        # And the farm
        farm = RectangularFarm(domain,
                               site_x_start=site_x_start,
                               site_x_end=site_x_start+site_x,
                               site_y_start=site_y_start,
                               site_y_end=site_y_start+site_y,
                               turbine=turbine)

        if turbine_positions:
            for i in range(len(turbine_positions)):
                farm.add_turbine(turbine_positions[i])
            farm.update()

        elif nx and ny:
            farm.add_regular_turbine_layout(num_x=nx, num_y=ny)

        prob_params.tidal_farm = farm

        # Set up the problem
        problem = SWProblem(prob_params)

        # Set up the solver parameters
        sol_params = CoupledSWSolver.default_parameters()
        sol_params.dump_period = 1
        sol_params.print_individual_turbine_power = True
        solver = CoupledSWSolver(problem, sol_params)

###### TESTING SOLVER - REMOVE WHOLE SECTION FOR RUN #####
#        f_state = File("results/state.pvd")
#
#        timer = Timer('')
        # To save memory, we deactivate the adjoint model with annotate=False.
        # We do not need the adjoint because we will not solve an optimisation problem
        # or compute sensitivities
#        for sol in solver.solve(annotate=False):
#            simulation_time = float(sol["time"])
#            log(INFO, "Computed solution at time %f in %f s." % (simulation_time, timer.stop()))
#            f_state << (sol["state"], simulation_time)
#            timer.start()
###### TESTING SOLVER - REMOVE WHOLE SECTION FOR RUN #####


        # Create the functional and reduced functional
        functional = PowerFunctional(problem, cut_in_speed=1., cut_out_speed=2.5)
	#functional = PowerFunctional(problem)
        control = TurbineFarmControl(farm)
        rf_params = ReducedFunctional.default_parameters()
        if mode=="sensitivity":
            rf_params.save_checkpoints = False
            rf_params.automatic_scaling = None
        else:
            rf_params.save_checkpoints = True
        rf = ReducedFunctional(functional, control, solver, rf_params)

        print '#!#!#', run_name, '#!#!#'
        print '!!!-!', other_info, '!!!-!'
        print rf_params

        if mode=="evaluate":
            rf.evaluate(farm.turbine_positions)
            power = rf.last_j
            print turbine_positions
            print 'Power output of ', run_name, 'is ', power
            return power

        elif mode=="sensitivity":
            print "="*20
            print "Evaluate power production"
            rf.evaluate(farm.turbine_positions)
            print "="*20
            print "Evaluate derivative"
            dj = rf.derivative(farm.turbine_positions, forget=False)
            power = rf.last_j
            print turbine_positions
            print 'Power output of ', run_name, 'is ', power
            print "Turbine positions: ", farm.turbine_positions
            print "Turbine sensitivities: ", dj

            # Compute sensitivity wrt to bottom friction
            print "="*20
            print "Evaluate derivative wrt bottom friction"
            control = Control(prob_params.friction)
            rf = FenicsReducedFunctional(functional, control, solver)
            #j = rf.evaluate()
            dj = rf.derivative(project=True, forget=False)
            File("dJ_dfriction.pvd") << dj

            control = Control(prob_params.depth)
            print "="*20
            print "Evaluate derivative wrt depth"
            rf = FenicsReducedFunctional(functional, control, solver)
            #j = rf.evaluate()
            dj = rf.derivative(project=True, forget=False)
            File("dJ_ddepth.pvd") << dj

            return power


        elif mode=="optimise":
            lb, ub = farm.site_boundary_constraints()
            ineq = farm.minimum_distance_constraints()
            f_opt = maximize(rf,
                             bounds=[lb, ub],
                             constraints=ineq,
                             method="SLSQP",
                             options={'maxiter': max_iterations})
            print 'FINAL POWER FOR TURBINES IS %f IN %i ITERATIONS \n' % (rf.last_j, max_iterations), run_name, other_info

if __name__ == "__main__":
    start_x = 491744.
    start_y = 6.5015e+06
    end_x = start_x+1000.
    end_y = start_y+600.
    channel_length = 1000.
    channel_width = 600.
    long_vel=2.
    trans_vel=0.
    viscosity=5.

otf = OTF(channel_length=channel_length,
          channel_width=channel_width,
          start_x=start_x, end_x=end_x,
          start_y=start_y, end_y=end_y,
          long_vel=long_vel, trans_vel=trans_vel,
          viscosity=viscosity)

otf.run(nx=5, ny=11,
        turbine_positions=None,
        mode="sensitivity",
        other_info='55 = 5x11',
        run_name='55=5x11',
        max_iterations=200)

