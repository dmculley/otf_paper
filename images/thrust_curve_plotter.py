import numpy as np
from matplotlib import pyplot as plt
from scipy.interpolate import interp1d
from dolfin import *
import math

class NonAnalyticThrustCurve(object):
    
    def __init__(self, 
                 cut_in_speed=1, 
                 cut_out_speed=2.5, 
                 rated_ct=0.6,
                 tcs=[]):
        
        self.cut_in_speed = cut_in_speed
        self.cut_out_speed = cut_out_speed
        self.rated_ct = rated_ct
        self.tcs=tcs
                 
    def martin_short_curve(self, u):
        power = []        
        for i in u:
            if i <= self.cut_in_speed:
                power.append(0)
            elif self.cut_in_speed < i <= self.cut_out_speed:
                power.append(self.rated_ct)
            else:
                power.append(self.rated_ct * (self.cut_out_speed**3 / i**3))
        return power
        
    def default_curve(self, u):
        power = []        
        for i in u:
            if i <= self.cut_in_speed+1:
                #from IPython import embed; embed()
                power.append(self.rated_ct*((math.tanh(10*(i-self.cut_in_speed))+1)/2.))
            elif self.cut_in_speed+1 < i <= self.cut_out_speed:
                power.append(self.rated_ct)
            else:
                power.append(self.rated_ct * (self.cut_out_speed**3 / i**3))
        return power
    
    def thrust_curve_samples(self, u):
        samples= [[self.cut_in_speed, 0], 
                  [self.cut_in_speed, self.rated_ct],
                  [self.cut_out_speed, self.rated_ct]]
        samples = samples + self.tcs
        return zip(*samples)
        
    def implemented_thrust(self, u, plot=False):
        u_max = max(u)
        unused_mesh_depth = 2
        mesh = UnitSquareMesh(len(u), unused_mesh_depth)
        V = FunctionSpace(mesh, "DG", 0)
        W = FunctionSpace(mesh, "CG", 1)
        x = triangle.x
        u = Expression('x[0]*u_max', u_max=u_max)
        u_func = project(u, W)
        
        def linear_interpolator(lower_cp, upper_cp, u_func):
            """ Join the steps between each checkpoint by a straight line.
            """
            return lower_cp[1] + (upper_cp[1] - lower_cp[1]) * \
                   (u_func - lower_cp[0]) / (upper_cp[0] - lower_cp[0])
        
        # First the bit before we cut in - we'll use tanh to keep it smooth
        a = conditional(gt(self.cut_out_speed, u_func), 
                        self.rated_ct*((tanh(10*(u_func-self.cut_in_speed))+1)/2),
                        1)
        # And now we linearly interpolate between the checkpoints
        b = conditional(gt(u_func, self.cut_out_speed), 
                        conditional(gt(self.tcs[0][0], u_func), 
                                    linear_interpolator([self.cut_out_speed, 
                                                        self.rated_ct], 
                                                        self.tcs[0], u_func),
                                    1),
                        1)                        
        c = conditional(gt(u_func, self.tcs[0][0]), 
                        conditional(gt(self.tcs[1][0], u_func),
                                    linear_interpolator(self.tcs[0], 
                                                        self.tcs[1], u_func),
                                    1), 
                        1)
        d = conditional(gt(u_func, self.tcs[1][0]), 
                        conditional(gt(self.tcs[2][0], u_func), 
                                    linear_interpolator(self.tcs[1],
                                                        self.tcs[2], u_func),
                                    1), 
                        1)
        e = conditional(gt(u_func, self.tcs[2][0]), 
                        conditional(gt(self.tcs[3][0], u_func), 
                                    linear_interpolator(self.tcs[2],
                                                        self.tcs[3], u_func),
                                    1), 
                        1)
        f = conditional(gt(u_func, self.tcs[3][0]), 
                        conditional(gt(self.tcs[4][0], u_func), 
                                    linear_interpolator(self.tcs[3],
                                                        self.tcs[4], u_func),
                                    linear_interpolator(self.tcs[4],
                                                        [10,0.0156], u_func)), 
                        1)                              
        ct_func = project(Constant(1), W)
        ct_func = ct_func * a * b * c * d * e * f

        if plot:
            ct_implemented = project(ct_func, W)
            ct_implemented = ct_implemented.vector().array()
            return ct_implemented[0:len(ct_implemented)/(unused_mesh_depth+1)-1]

    def power(self, u, ct):
        power = []
        for i in range(len(u)):
            power.append(0.5 * 1000. * ct[i] * u[i]**3 * 314 * 1e-6)
        return power
        
        
if __name__ == '__main__':
    
    plot_power = True
    plot_thrust = False
    save = True
    
    u = np.linspace(0, 6, 1000)
    cut_in_speed = 1
    cut_out_speed = 2.5
    rated_ct = 0.6
    tcs = [[3.0, 0.347], [3.5, 0.219], [4.0,0.146], [4.5, 0.1029], [5, 0.075]]
    
    TC = NonAnalyticThrustCurve(cut_in_speed, cut_out_speed, rated_ct, tcs)

    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')

    fig = plt.figure(figsize=(5,5.5))
    ax1 = plt.subplot(1, 1, 1)
    
    if plot_thrust:
        ax1.plot(u, TC.default_curve(u), label="$c_t(u)$", color='black')    
        ax1.plot([1,1],[0,0.6], ':', label='$u_{\mathrm{in}}$', color='black')
        ax1.plot([2.5,2.5],[0,0.6], '-.', label='$u_{\mathrm{rated}}$', color='black')
        ax1.plot([0,6],[0.6,0.6], '--', label='$c_t^{\mathrm{rated}}$', color='black')
        ax1.set_xlabel('Flow speed, $u$, ms$^{-1}$')
        ax1.set_ylabel('Thrust coefficient, $C_t$')
        ax1.set_aspect(6/0.8)    
        ax1.set_ylim([0,0.8])    
        ax1.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
               ncol=2, mode="expand", borderaxespad=0.)
        if save:
            plt.savefig('thrust_curve.png', dpi=600, transparent=True)
        else: plt.show()
               
    if plot_power:
        power = TC.power(u, TC.default_curve(u))
        ax1.plot(u, power, label="$P(u)$", color='black')    
        ax1.plot([1,1],[0,max(power)], ':', label='$u_{\mathrm{in}}$', color='black')
        ax1.plot([2.5,2.5],[0,max(power)], '-.', label='$u_{\mathrm{rated}}$', color='black')
        ax1.plot([0,6],[max(power),max(power)], '--', label='$P_{\mathrm{rated}}$', color='black')
        ax1.set_xlabel('Flow speed, $u$, ms$^{-1}$')
        ax1.set_ylabel('Power, $P$')
        ax1.set_aspect(6/1.6)    
        ax1.set_ylim([0,1.6])    
        ax1.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
               ncol=2, mode="expand", borderaxespad=0.)
        if save:
            plt.savefig('power_curve.png', dpi=600, transparent=True)
        else: plt.show()